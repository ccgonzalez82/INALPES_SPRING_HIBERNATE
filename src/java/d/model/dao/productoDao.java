/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d.model.dao;
import d.model.pojos.Inmueble;
import d.model.pojos.Persona;
import d.model.pojos.Sucursal;
import d.model.pojos.TipoInmueble;
import d.model.pojos.TipoPersona;
import d.model.pojos.Ubicacion;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;
import java.util.List;
/**
 *
 * @author KGB DELTA JM
 */
public class productoDao {
    
    
    public void ingresarPersona(Persona p){
        SessionFactory sf=null;
        Session sesion=null;
        Transaction tx= null;
        
        try{
            sf = HibernateUtil.getSessionFactory();
            sesion = sf.openSession();
            tx = sesion.beginTransaction();
            sesion.save(p);
            tx.commit();
            sesion.close();
        }
        catch(Exception ex){
            tx.rollback();
            throw new RuntimeException("No se pudo guardar");
        }
    }
    
    public String consultarPersona(int codigo){
        SessionFactory sf =   HibernateUtil.getSessionFactory();
        Session sesion=  sf.openSession();
        Persona p = (Persona)sesion.get(Persona.class, codigo);
        sesion.close();
        if(p!=null){
            return "Identificacion" + p.getIdentificacion() + "nommbre1"+ p.getNombre1() + "nombre2"+ p.getNombre2()+
                    "apellido1"+p.getApellido1()+"apellido2"+p.getApellido2()+"genero"+p.getGenero()+"Tipo de persona"+p.getTipoPersona()+
                    "Fecha de nacimiento"+p.getFechaNacimiento()+"Telefono"+p.getTelefono()+"Correo"+p.getEmail();
        }
        else{
            return "El codigo "+ codigo+ "no esta registrado";
        }
    }
    public List<Persona> verProductos(){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.openSession();
        Query query = sesion.createQuery("from Persona");
        List<Persona> lista = query.list();
        sesion.close();
        
        return lista;
    }
         
}
